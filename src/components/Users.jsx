import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getUsers } from "../redux/actions/users";
import Card from "./Card";

const Users = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users.users);
  const loading = useSelector((state) => state.users.loading);
  const error = useSelector((state) => state.users.error);

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  return (
    <>
      {users.loading && <h1>loading...</h1>}

      {users?.length > 0 &&
        users?.map((user) => {
          return <Card key={user.id} user={user} />;
        })}

      {users?.length === 0 && <h1>No user available</h1>}

      {error && !loading && <p>{error}</p>}
    </>
  );
};

export default Users;
